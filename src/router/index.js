import Vue from 'vue'
import Router from 'vue-router'
import ExchangeRates from '@/components/ExchangeRates'
import Main from '@/components/Main'
import Result from '@/components/Result'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/select_exchanges',
      name: 'ExchangeRates',
      component: ExchangeRates
    },
    {
      path: '/result',
      name: 'Result',
      component: Result,
      props: true
    }
  ]
})
