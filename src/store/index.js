import Vuex from 'vuex'
import Vue from 'vue'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    fullResponse: axios.get('https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11')
      .then(response => response),
    amount: null,
    historyList: JSON.parse(localStorage.getItem('conversions')) || []
  },
  getters: {
    getDataFromResponse: state => state.fullResponse.then(response => response.data)
  },
  mutations: {
    setAmount (state, value) {
      state.amount = value
    },
    updateHistory (state, value) {
      state.historyList.push(value)
    }
  }
})
